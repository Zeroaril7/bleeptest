
class Query {

  constructor(db) {
    this.db = db;
  }

  async findOneParticipant(parameter) {
    const { name } = parameter;
    const recordset = await this.db.prepareQuery('SELECT name, age, gender, level, shuttle FROM result_test WHERE name = ?', [name]);
    return recordset;
  }

  async findById(id) {
    const recordset = await this.db.prepareQuery('SELECT name, age, gender, level, shuttle FROM result_test WHERE id = ?', [id]);
    return recordset;
  }

}

module.exports = Query;
