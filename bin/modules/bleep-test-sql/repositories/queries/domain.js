const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError } = require('../../../../helpers/error');

class BleepTest {

  constructor(db){
    this.query = new Query(db);
  }

  async viewParticipant(id) {
    const participant = await this.query.findById(id);
    if (participant.err) {
      return wrapper.error(new NotFoundError('Can not find participant'));
    }
    const { data } = participant;
    return wrapper.data(data);
  }


}

module.exports = BleepTest;
