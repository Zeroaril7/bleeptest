class Command {

  constructor(db) {
    this.db = db;
  }

  async insertOneTest(document){
    const {id, name, age, gender, level, shuttle} = document;
    const result = await this.db.prepareQuery('INSERT INTO result_test (id, name, age, gender, level, shuttle) VALUES (?, ?, ?, ?, ?, ?)',
      [id, name, age, gender, level, shuttle]);
    return result;
  }

  async deleteOneParticipant(id){
    const result = await this.db.prepareQuery('DELETE FROM result_test WHERE id = ?', id);
    return result;
  }

  async updateOneTest(document, id){
    const { name, age, gender, level, shuttle } = document;
    const result = await this.db.prepareQuery('UPDATE result_test SET name = ?, age = ?, gender = ?, level = ?, shuttle = ? WHERE id = ?',
      [name, age, gender, level, shuttle, id]);
    return result;
  }
}

module.exports = Command;
