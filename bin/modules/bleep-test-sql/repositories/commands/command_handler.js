
const BleepTest = require('./domain');
const MySql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');
const db = new MySql(config.get('/mysqlConfig'));

const postDataTest = async (payload) => {
  const bleepTest = new BleepTest(db);
  const postCommand = async payload => bleepTest.addFormTest(payload);
  return postCommand(payload);
};

const putDataTes = async (payload, id) => {
  const bleepTest = new BleepTest(db);
  const postCommand = async (payload, id)  => bleepTest.update(payload, id);
  return postCommand(payload, id);
};

const deleteDataTest = async (id) => {
  const bleepTest = new BleepTest(db);
  const deleteCommand = async (id) => bleepTest.delete(id);
  return deleteCommand(id);
};


module.exports = {
  postDataTest,
  putDataTes,
  deleteDataTest
};
