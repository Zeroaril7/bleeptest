const ObjectId = require('mongodb').ObjectId;

class Command {

  constructor(db) {
    this.db = db;
  }

  async insertOneTest(document){
    this.db.setCollection('result_test');
    const result = await this.db.insertOne(document);
    return result;
  }

  async deleteOneTest(id){
    this.db.setCollection('result_test');
    const parameter = {
      _id: ObjectId(id),
    };
    const result = await this.db.deleteOne(parameter);
    return result;
  }

  async updateOneTest(payload, id){
    this.db.setCollection('result_test');
    const result = await this.db.upsertOne({ _id: ObjectId(id) }, { $set: payload });
    return result;
  }
}

module.exports = Command;
