
const BleepTest = require('./domain');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');
const db = new Mongo(config.get('/mongoDbUrl'));

const postDataTest = async (payload) => {
  const bleepTest = new BleepTest(db);
  const postCommand = async payload => bleepTest.addFormTest(payload);
  return postCommand(payload);
};

const putDataTest = async (payload, id) => {
  const bleepTest = new BleepTest(db);
  const postCommand = async (payload,id) => bleepTest.update(payload, id);
  return postCommand(payload, id);
};

const deleteTest = async (id) => {
  const bleepTest = new BleepTest(db);
  const deleteCommand = async (id) => bleepTest.delete(id);
  return deleteCommand(id);
};

module.exports = {
  postDataTest,
  putDataTest,
  deleteTest
};
