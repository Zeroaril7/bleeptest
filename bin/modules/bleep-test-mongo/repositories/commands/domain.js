const Query = require('../queries/query');
const Command = require('./command');
const Model = require('./command_model');
const wrapper = require('../../../../helpers/utils/wrapper');
const jwtAuth = require('../../../../auth/jwt_auth_helper');
const commonUtil = require('../../../../helpers/utils/common');
const logger = require('../../../../helpers/utils/logger');
const { NotFoundError, UnauthorizedError, ConflictError } = require('../../../../helpers/error');
const { randomUUID } = require('crypto');

const algorithm = 'aes-256-ctr';
const secretKey = 'Dom@in2018';

class BleepTest {

  constructor(db){
    this.command = new Command(db);
    this.query = new Query(db);
  }

  async addFormTest(payload) {
    const { name, age, gender, level, shuttle } = payload;
    const participant = await this.query.findOneParticipant({ name });

    if (participant.data) {
      return wrapper.error(new ConflictError('participant already exist'));
    }

    const addDataTest = Model.AddFormTest();
    addDataTest.id = randomUUID();
    addDataTest.name = name;
    addDataTest.age = age;
    addDataTest.gender = gender;
    addDataTest.level = level;
    addDataTest.shuttle = shuttle;

    const { data:result } = await this.command.insertOneTest(addDataTest);
    return wrapper.data(result);

  }

  async update(payload, id) {
    const result = await this.command.updateOneTest(payload, id);
    if (result.err) {
      return wrapper.error(new ConflictError('test failed to update'));
    }
    return wrapper.data(result);
  }

  async delete(id) {
    const result = await this.command.deleteOneTest(id);
    if (result.err) {
      return wrapper.error(new ConflictError('test failed to delete'));
    }
    return wrapper.data(result);
  }

}

module.exports = BleepTest;
