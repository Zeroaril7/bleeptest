const joi = require('joi');
const uuid = require('uuid');

const formTest = joi.object({
  name: joi.string().required(),
  age: joi.number().required(),
  gender: joi.string().required(),
  level: joi.number().required(),
  shuttle: joi.number().required(),
});

const AddFormTest = () => {
  return {
    id: uuid(),
    name: '',
    age: 0,
    gender: '',
    level: 0,
    shuttle: 0
  };
};

module.exports = {
  formTest,
  AddFormTest
};
