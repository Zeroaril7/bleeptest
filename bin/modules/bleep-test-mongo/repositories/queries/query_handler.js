
const BleepTest = require('./domain');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');
const db = new Mongo(config.get('/mongoDbUrl'));
const bleepTest = new BleepTest(db);

const getTest = async (id) => {
  const getData = async () => {
    const result = await bleepTest.viewTest(id);
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  getTest
};
