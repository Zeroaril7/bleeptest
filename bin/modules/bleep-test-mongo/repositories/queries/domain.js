
const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError } = require('../../../../helpers/error');

class BleepTest {

  constructor(db){
    this.query = new Query(db);
  }

  async viewTest(id) {
    const bleepTest = await this.query.findById(id);
    if (bleepTest.err) {
      return wrapper.error(new NotFoundError('Can not find Test Data'));
    }
    const { data } = bleepTest;
    return wrapper.data(data);
  }

}

module.exports = BleepTest;
