
const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const { ERROR:httpError, SUCCESS:http } = require('../../../helpers/http-status/status_code');

const postDataTest = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, commandModel.formTest);
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.postDataTest(result.data);
  };

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Add Data Test')
      : wrapper.response(res, 'success', 'Add Data Test', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const getDataTest = async (req, res) => {
  const { id } = req.params;
  const getData = async () => queryHandler.getTest(id);
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Get Test', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', result, 'Get Test', http.OK);
  };
  sendResponse(await getData());
};

const putDataTes = async (req, res) => {
  const payload = req.body;
  const { id } = req.params;
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.putDataTest(payload, id);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Update Test Data', httpError.CONFLICT)
      : wrapper.response(res, 'success', 'Update Test Data', http.OK);
  };
  sendResponse(await postRequest(payload, id));
};

const deleteDataTest = async (req, res) => {
  const { id } = req.params;
  const deleteTest = async => commandHandler.deleteTest(id);
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Delete Participant', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', 'Delete Participant', http.OK);
  };
  sendResponse(await deleteTest());
};


module.exports = {
  postDataTest,
  getDataTest,
  putDataTes,
  deleteDataTest
};
