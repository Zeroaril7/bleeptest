class Command {

  constructor(db) {
    this.db = db;
  }

  async insertOneUser(document){
    const {id, username, password} = document;
    const result = await this.db.prepareQuery('INSERT INTO user (id, username, password) VALUES (?, ?, ?)',
      [id, username, password]);
    return result;
  }

  async deleteOneUser(userId){
    const result = await this.db.prepareQuery('DELETE FROM user WHERE id = ?', [userId]);
    return result;
  }
}

module.exports = Command;
