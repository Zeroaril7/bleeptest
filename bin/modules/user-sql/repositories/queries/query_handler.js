
const User = require('./domain');
const MySql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');
const db = new MySql(config.get('/mysqlConfig'));
const user = new User(db);

const getUser = async (id) => {
  const getData = async () => {
    const result = await user.viewUser(id);
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  getUser
};
