
const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
const jwtAuth = require('../auth/jwt_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const userMongoHandler = require('../modules/user-mongo/handlers/api_handler');
const userSqlHandler = require('../modules/user-sql/handlers/api_handler');
const bleepTestMongoHandler = require('../modules/bleep-test-mongo/handlers/api_handler');
const bleepTestSqlHandler = require('../modules/bleep-test-sql/handlers/api_handler');
const mongoConnectionPooling = require('../helpers/databases/mongodb/connection');
const mysqlConnectionPooling = require('../helpers/databases/mysql/connection');

function AppServer() {
  this.server = restify.createServer({
    name: `${project.name}-server`,
    version: project.version
  });

  this.server.serverKey = '';
  this.server.use(restify.plugins.acceptParser(this.server.acceptable));
  this.server.use(restify.plugins.queryParser());
  this.server.use(restify.plugins.bodyParser());
  this.server.use(restify.plugins.authorizationParser());

  // required for CORS configuration
  const corsConfig = corsMiddleware({
    preflightMaxAge: 5,
    origins: ['*'],
    // ['*'] -> to expose all header, any type header will be allow to access
    // X-Requested-With,content-type,GET, POST, PUT, PATCH, DELETE, OPTIONS -> header type
    allowHeaders: ['Authorization'],
    exposeHeaders: ['Authorization']
  });
  this.server.pre(corsConfig.preflight);
  this.server.use(corsConfig.actual);

  // // required for basic auth
  this.server.use(basicAuth.init());

  // anonymous can access the end point, place code bellow
  this.server.get('/', (req, res) => {
    wrapper.response(res, 'success', wrapper.data('Index'), 'This service is running properly');
  });

  // User Mongo
  this.server.post('/api/users/mongo/v1', basicAuth.isAuthenticated, userMongoHandler.postDataLogin);
  this.server.get('/api/users/mongo/v1/:userId', jwtAuth.verifyTokenMongo, userMongoHandler.getUser);
  this.server.post('/api/users/mongo/v1/register', basicAuth.isAuthenticated, userMongoHandler.registerUser);
  this.server.del('/api/users/mongo/v1/:userId', jwtAuth.verifyTokenMongo, userMongoHandler.deleteUser);

  // User Sql
  this.server.post('/api/users/sql/v1', basicAuth.isAuthenticated, userSqlHandler.postDataLogin);
  this.server.get('/api/users/sql/v1/:userId', jwtAuth.verifyTokenSql, userSqlHandler.getUser);
  this.server.post('/api/users/sql/v1/register', basicAuth.isAuthenticated, userSqlHandler.registerUser);
  this.server.del('/api/users/sql/v1/:userId', jwtAuth.verifyTokenSql, userSqlHandler.deleteUser);

  // Bleep Test Mongo
  this.server.post('/api/bleep-tests/mongo/v1', jwtAuth.verifyTokenMongo, bleepTestMongoHandler.postDataTest);
  this.server.get('/api/bleep-tests/mongo/v1/:id', basicAuth.isAuthenticated, bleepTestMongoHandler.getDataTest);
  this.server.del('/api/bleep-tests/mongo/v1/:id', jwtAuth.verifyTokenMongo, bleepTestMongoHandler.deleteDataTest);
  this.server.put('/api/bleep-test/mongo/v1/:id', jwtAuth.verifyTokenMongo, bleepTestMongoHandler.putDataTes);

  // Bleep Test Sql
  this.server.post('/api/bleep-tests/sql/v1', jwtAuth.verifyTokenSql, bleepTestSqlHandler.postDataTest);
  this.server.get('/api/bleep-tests/sql/v1/:id', basicAuth.isAuthenticated, bleepTestSqlHandler.getDataTest);
  this.server.del('/api/bleep-tests/sql/v1/:id', jwtAuth.verifyTokenSql, bleepTestSqlHandler.deleteDataTest);
  this.server.put('/api/bleep-test/sql/v1/:id', jwtAuth.verifyTokenSql, bleepTestSqlHandler.putDataTes);

  //Initiation
  mongoConnectionPooling.init();
  mysqlConnectionPooling.createConnectionPool;
}

module.exports = AppServer;
